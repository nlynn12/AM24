-- Project:     AM24: Examination Project: Ore Collection on Foreign Planets
-- File:        master.lua
-- Author:      Nicolai A. Lynnerup

core    = require "ranalib_core"
agent   = require "ranalib_agent"
shared  = require "ranalib_shared"
stat    = require "ranalib_statistic"
map     = require "ranalib_map"

function initializeAgent()
    -- DEBUG --
    say("Master Agent #" .. ID .. " is initialized")

    T = 100000                -- Maximum # of cycles
    t_count = 0

    -- Shared Parameters --
    shared.storeTable("world",{
        G = ENV_WIDTH,      -- 2D torus surface (GxG)
        D = 0.02,           -- Ore distribution on planet with density D
        Z = 5,              -- Number of companies
        M = 1,              -- Coordination mode (boolean, true: all companies cooperates, false: all companies competes)
        N = 1,              -- Landbases pr. company
        C = 500,            -- Ore capacity pr. landbase
        X = 4,              -- Number of explorers pr. base
        Y = 4               -- Number of transporters pr. base
    })

    shared.storeTable("robot",{
        P = 11,             -- Perceptionscope (PxP), P furthermore denotes the cost of perception
        I = 40,             -- Communication scope (IxI)
        Q = 1,              -- Cost of motion
        M = 1,              -- Communication cost
        E = 500,           -- Energy capacity
        EC = 100,           -- Charging rate pr. step
        W = 4               -- Ore capacity pr. transporter
    })

    shared.storeTable("color",{
        ore           = {R = 255,   G = 255, B = 0  },
        base          = {r = 0,     g = 0,   b = 200},
        transporter   = {r = 255,   g = 255, b = 255},
        explorer      = {r = 0,     g = 200, b = 0  }
    })

    shared.storeNumber("dataCollection",false)
    shared.storeNumber("timeStamp",os.time())


    -- Add ores --
    addOres()

    -- Add Companies --
    local nCompanies = shared.getTable("world")["Z"]
    for i=1, nCompanies do
        agent.addAgent("company.lua")--, 20+i*50, 20+i*50)
    end
end

function takeStep()
    -- Remove Master Agent
    --agent.removeAgent(ID)

    -- Move master agent out of scope
    agent.changeColor({r=0,g=0,b=0})
    PositionX = -10
    PositionY = ENV_HEIGHT/2

    -- Control Total Number of Cycles
    t_count = t_count + 1
    if(t_count >= T) then
        core.stopSimulation()
    end
end

function cleanUp()
    found = 0
    remaining = 0
    for x=1,ENV_WIDTH do
        for y=1,ENV_HEIGHT do
            maprgb = map.checkColor(x,y)
            if(maprgb.R == 255 and maprgb.G == 0 and maprgb.B == 0) then
                found = found + 1
            elseif (maprgb.R == 255 and maprgb.G == 255 and maprgb.B == 0) then
                remaining = remaining + 1
            end
        end
    end

    say(found .. "," .. remaining)

    say("Clean up")
    --file = io.open("data.csv", "w")
    --for i,v in pairs(Olevels) do
    --    file:write(i..","..v.."\n")
    --end
    --file:close()
end

function addOres()
    oreColor = shared.getTable("color")["ore"]

    local RGB = {R=0, G=0, B=0}
    for i=1, ENV_WIDTH*ENV_HEIGHT*shared.getTable("world")["D"] do
        repeat
            x = stat.randomInteger(0,ENV_WIDTH)
            y = stat.randomInteger(0,ENV_HEIGHT)
            RGB = map.checkColor(x,y)
        until ((RGB.R ~= oreColor.R) or (RGB.G ~= oreColor.G) or (RGB.B ~= oreColor.B))
        map.modifyColor(x,y,oreColor.R,oreColor.G,oreColor.B)
    end
end
