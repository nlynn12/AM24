-- Project:     AM24: Examination Project: Ore Collection on Foreign Planets
-- File:        explorer.lua
-- Author:      Nicolai A. Lynnerup

-- RanaLib
agent       = require "ranalib_agent"
shared      = require "ranalib_shared"
stat        = require "ranalib_statistic"
collision   = require "ranalib_collision"
draw        = require "ranalib_draw"
map         = require "ranalib_map"
event       = require "ranalib_event"

-- Own
move        = require "lib_move"
world       = require "lib_torus"

joined = false

function initializeAgent()
    say("Explorer Agent #" .. ID .. " is deployed")

    -- Change color of explorer unit
    agent.changeColor(shared.getTable("color")["explorer"])

    -- Save Home Position
    home = {x = PositionX, y = PositionY}

    -- State machine
    states = {
        explore = explore,
        perceive = perceive,
        in_base = in_base,
        gohome = gohome
    }

    -- Initial State --
    curr_state = "in_base"

    -- Retrieve parameters
    Q = shared.getTable("robot")["Q"]
    P = shared.getTable("robot")["P"]
    X = shared.getTable("world")["X"]
    Y = shared.getTable("world")["Y"]
    M = shared.getTable("robot")["M"]
    E = shared.getTable("robot")["E"]
    I = shared.getTable("robot")["I"]
    groupID = shared.getNumber("groupID")

    -- Initialization of Variables
    p_step = P
    m_step = 10
    x = stat.randomInteger(0, ENV_WIDTH)
    y = stat.randomInteger(0, ENV_HEIGHT)
    memory = {}
    memory_capacity = X+Y

    -- Data Collection only
    dataCollection = shared.getNumber("dataCollection")
    dataStep = {"step"}
    dataE = {"energy"}
    dataState = {"state"}
    dataMem = {"memory"}
    dataTot = {"total"}

    step = 0
    foundTotal = 0
    we_are_leaving = false
end

function handleEvent(sourceX, sourceY, sourceID, eventDescription, eventTable)
    if eventTable.id == groupID and eventDescription == "explorer" then
        if world.distance({x = PositionX, y = PositionY},{x = sourceX, y = sourceY}) < I then
            -- Send list of coordinates on request
            if #memory > 0 then
                event.emit{targetID=sourceID, speed=0, description="transporter",table={id=groupID,coordinates=memory}} --, targetGroup=groupID
            end
        end
    elseif eventTable.id == groupID and eventDescription == "wanted" then
        if world.distance({x = PositionX, y = PositionY},{x = sourceX, y = sourceY}) < I then
            -- Check if coordinate still exists
            tr_coordinates = eventTable.coordinates
            coordinate_exists = {}
            for i=1,#tr_coordinates do
                coordinate_exists[i] = false
            end


            if #memory ~= 0 then
                for i=1,#tr_coordinates do
                    for j=1, #memory do
                        if coordinate_exists[i] == false then
                            if tr_coordinates[i].x == memory[j].x and tr_coordinates[i].y == memory[j].y then
                                coordinate_exists[i] = true
                            end
                        end
                    end
                end
            end

            -- Create return list for transporter
            local coor_for_tr = {}
            for i=1,#coordinate_exists do
                if coordinate_exists[i] then
                    table.insert(coor_for_tr,tr_coordinates[i])
                end
            end

            -- Update explorer's memory
            for i=#memory,1,-1 do
                if coordinate_exists[i] then
                    table.remove(memory,i)
                end
            end

            -- Emit acknowledge to transporter
            event.emit{targetID=sourceID, speed=0, description="acknowledge",table={id=groupID,coordinates=coor_for_tr}}
        end
    elseif eventTable.id == groupID and eventDescription == "comehome" then
        -- No check for I (cheating I know)
        curr_state = "gohome"
        we_are_leaving = true
    end
end

function takeStep()
    --if E < 0 then
    --    say(ID .. " energy:" .. E .. "    state: " .. curr_state .. "    move_state: "..curr_move_state .. "   mem:" .. #memory)
    --end

    if not joined then
        -- Join group
        agent.joinGroup(groupID)
        say("Agent "..ID.." joined group "..groupID)
        joined = true
    end

    if dataCollection then
        table.insert(dataStep,step)
        table.insert(dataE,E)
        table.insert(dataMem,#memory)
        table.insert(dataState,curr_state)
        table.insert(dataTot,foundTotal)
        step = step + 1
    end

    states[curr_state]()
end


-- STATE 1.1
function explore()
    -- Check if there's sufficient energy to perform move action and still be alive
    local cost_move_home = (world.distance({x = PositionX, y = PositionY},home)*Q)
    if(cost_move_home >= (E - Q)) or (#memory == memory_capacity) then
        curr_state = "gohome"
        states[curr_state]()
    else
        -- Get random coordinate
        if (l_getMersenneInteger(1,100) <= 1) or (PositionX == x and PositionY == y) then
            x = stat.randomInteger(0, ENV_WIDTH)
            y = stat.randomInteger(0, ENV_HEIGHT)
        end

        -- Move robot towards random coordinate
        move.towards({x = x, y = y})
        E = E - Q

        -- Decrement perceive step counter
        p_step = p_step - 1

        -- Robot checks if it should perform a perception step or keep moving
        if(p_step == 0) then
            -- Check if robot can get home after possible perception
            local cost_move_home = (world.distance({x = PositionX, y = PositionY},home)*Q)
            if(cost_move_home >= (E - P)) then
                curr_state = "gohome"
            else
                curr_state = "perceive"
            end
            -- Reset step counter
            p_step = P
        end
    end
end

-- STATE 1.2
function gohome()
    if(PositionX == home.x and PositionY == home.y) then
        -- Go into charging mode
        curr_state = "in_base"
        states[curr_state]()
    else
        move.towards(home)
        E = E - Q
    end
end

-- STATE 2
function perceive()
    --inCollision()

    -- Draw
    --edge = {255,255,255}
    --draw.setColors(edge,edge)
    draw.circle(PositionX,PositionY,P/2,false)
    local radial = map.getRadialMask(P/2)
    local ore_color = shared.getTable("color")["ore"]
    local ore_found = false
    for i = 1, #radial do
        rgb = map.checkColor(radial[i].posX,radial[i].posY)
        if rgb.R == ore_color.R and rgb.G == ore_color.G and rgb.B == ore_color.B then
            -- DEBUG
            map.modifyColor(radial[i].posX,radial[i].posY,255,0,0)
            store_in_table(radial[i].posX,radial[i].posY)
            ore_found = true
            -- Data collection
            foundTotal = foundTotal + 1
        end
    end

    -- Decrement energy counter
    E = E - P

    curr_state = "explore"
end

-- STATE 4
function in_base()
    -- Retrieve max E level for robot
    local max_energy = shared.getTable("robot")["E"]

    if(E < max_energy) then
        E = E + shared.getTable("robot")["EC"]
        --say("Charging: " .. E)
    end

    if(E >= max_energy) then
        -- ensure no "overshoot"
        E = max_energy

        if(#memory < memory_capacity) and we_are_leaving == false then
            -- change state
            curr_state = "explore"
        end
    else
        curr_state = "in_base"
    end
end




function cleanUp()
    file = io.open("../data/" .. shared.getNumber("timeStamp") .. "_explorer"..ID.."_group".. groupID ..".csv", "w")
    for i=1,#dataStep do
        file:write(dataStep[i]..","..dataE[i]..","..dataState[i]..","..dataMem[i]..","..dataTot[i].."\n")
    end
    file:close()
end




-- Helper functions --

function store_in_table(x,y)
    -- Check if robot has sufficient memory to store coordinate
    --say("ID: " .. ID .. " mem.size: " .. #memory .. " (" .. memory_capacity .. ")")
    if #memory < memory_capacity then
        table.insert(memory,{x=x,y=y})
    else
        curr_move_state = "gohome"
    end
end

function check_energy_level(cost,alt_state)
    -- If robot is not going home then check it checks it E level
    if (curr_move_state ~= "gohome") then
        local dist_home = world.distance({x = PositionX, y = PositionY},home)
        local cost_move_home = (dist_home*Q) + math.ceil((dist_home/m_step)*M)
        if(cost_move_home > (E - cost)-50) then
            curr_move_state = "gohome"
        else
            if(alt_state == "explore") then
                curr_move_state = alt_state
            elseif(alt_state == "perceive") then
                curr_state = alt_state
            end
        end
    end
end

function inCollision()
    local table = collision.radialCollisionScan(P/2)
    if table then
        say("Agent: ".. ID.. " has these collisions")
        for i = 1, #table do
            say("\tAgent: "..table[i].id .. " at X:".. table[i].posX .. " Y:" .. table[i].posY)
        end
    end
end

