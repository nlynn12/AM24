-- Project:     AM24: Examination Project: Ore Collection on Foreign Planets
-- File:        transporter.lua
-- Author:      Nicolai A. Lynnerup

-- RanaLib
agent       = require "ranalib_agent"
shared      = require "ranalib_shared"
stat        = require "ranalib_statistic"
collision   = require "ranalib_collision"
draw        = require "ranalib_draw"
map         = require "ranalib_map"
event       = require "ranalib_event"

-- Own
move        = require "lib_move"
world       = require "lib_torus"

joined = false

function initializeAgent()
    say("Transporter Agent #" .. ID .. " is deployed")
    -- Change color of explorer unit
    agent.changeColor(shared.getTable("color")["transporter"])

    -- Save Home Position
    home = {x = PositionX, y = PositionY}

    -- State machine
    states = {
        moving = moving,
        collect = collect,
        movehome = movehome,
        interaction = interaction,
        in_base = in_base
    }

    curr_state = "in_base";

    -- Initialization of Variables
    x = 0
    y = 0
    capacity = 0
    memory = {}

    c_step = 10
    pop_new_coordinate = true


    -- Retrieve parameters
    Q = shared.getTable("robot")["Q"]
    X = shared.getTable("world")["X"]
    Y = shared.getTable("world")["Y"]
    E = shared.getTable("robot")["E"]
    W = shared.getTable("robot")["W"]
    M = shared.getTable("robot")["M"]
    I = shared.getTable("robot")["I"]
    groupID = shared.getNumber("groupID")

    memory_capacity = X+Y

    -- Data Collection only
    dataCollection = shared.getNumber("dataCollection")
    dataStep = {"step"}
    dataE = {"energy"}
    dataState = {"state"}
    dataMem = {"memory"}
    dataW = {"capacity"}
    dataTot = {"total"}

    step = 0
    collectedTotal = 0
    we_are_leaving = false
end

function handleEvent(sourceX, sourceY, sourceID, eventDescription, eventTable)
    --if eventDescription == "ping" and ID ~= 1 then
    if eventTable.id == groupID and eventDescription == "transporter" then
        if world.distance({x = PositionX, y = PositionY},{x = sourceX, y = sourceY}) < I then
            -- Inside com.scope

            if #memory < memory_capacity then
                ores = eventTable.coordinates
                table.sort(ores,compare_dist)

                temp_mem_size = #memory
                coor_wanted = {}
                for i=1,#ores do
                    if(temp_mem_size <= memory_capacity) then
                        table.insert(coor_wanted,ores[#ores])
                        --say("Agent ".. ID .. "taking: (" .. ores[#ores].x .. "," .. ores[#ores].y .. ")")
                        table.remove(ores)
                        temp_mem_size = temp_mem_size + 1
                    end
                end
                event.emit{targetID=sourceID, speed=0, description="wanted", table={id=groupID, coordinates=coor_wanted}}
            end
        end
    elseif eventTable.id == groupID and eventDescription == "acknowledge" then
        if world.distance({x = PositionX, y = PositionY},{x = sourceX, y = sourceY}) < I then
            -- Load coordinates into memory
            ores = eventTable.coordinates
            for i=1,#ores do
                table.insert(memory,ores[i])
            end
        end
    elseif eventTable.id == groupID and eventDescription == "comehome" then
        curr_state = "movehome"
        we_are_leaving = true
    end
end


function takeStep()
    if not joined then
        -- Join group
        agent.joinGroup(groupID)
        --say("Agent "..ID.." joined group "..groupID)
        joined = true
    end

    if dataCollection then
        table.insert(dataStep,step)
        table.insert(dataE,E)
        table.insert(dataMem,#memory)
        table.insert(dataState,curr_state)
        table.insert(dataW,capacity)
        table.insert(dataTot,collectedTotal)
        step = step + 1
    end

    --say(ID .. " " .. curr_state)
    states[curr_state]()
end

-- STATE 1
function collect()
    if pop_new_coordinate then
        -- Only pop new coordinate if there's storage space left
        if #memory > 0 and capacity < W then
            -- Sort coordinates to "minimize" transporters route (NB! NP-complete: traveling salesman problem...)
            table.sort(memory,compare_dist)

            -- Pop element from stack
            coordinate = memory[#memory]
            table.remove(memory)
            pop_new_coordinate = false
        else
            -- Oh.. Memory must be empty, or there's not sufficient storage.. Return to sender
            curr_state = "movehome"
        end
    else
        move.towards(coordinate)
        E = E - Q
        c_step = c_step - 1

        -- Have robot reached the ore?
        if(PositionX == coordinate.x and PositionY == coordinate.y) then
            maprgb = map.checkColor(PositionX,PositionY)
            -- Check if ore sample is still present
            if(maprgb.R == 255 and maprgb.G == 0 and maprgb.B == 0) then
                map.modifyColor(coordinate.x,coordinate.y,0,0,0)
                capacity = capacity + 1
                collectedTotal = collectedTotal + 1
            end
            -- Ore has been collected, get new coordinate
            pop_new_coordinate = true
        end

        if c_step == 0 and #memory < memory_capacity then
            c_step = 10
            curr_state = "interaction"
        end
    end
end

-- STATE 2
function movehome()
    if(PositionX == home.x and PositionY == home.y) then
        -- Go into offloading + charging mode
        curr_state = "in_base"
    else
        move.towards(home)
        curr_state = "movehome"
    end
end

-- STATE 3
function interaction()
    free_slots = memory_capacity - #memory
    event.emit{speed=0, description="explorer", table={id=groupID, free_slots=free_slots}}
    E = E - I
    curr_state = "collect"
end

-- STATE 4
function in_base()
    -- Offload in base
    if(capacity > 0) then
        event.emit{speed=0,description="",table={agentType="transporter",cmdType="offload",msg=capacity}}
        capacity = 0
    end

    -- Retrieve max energy level for robot
    local max_energy = shared.getTable("robot")["E"]

    if(E < max_energy) then
        E = E + shared.getTable("robot")["EC"]
    end

    if(E >= max_energy) and we_are_leaving == false then
        -- ensure no "overshoot"
        E = max_energy

        -- State change based on memory
        if #memory > 0 then
        --if #memory > memory_capacity*0.25 then
            pop_new_coordinate = true
            curr_state = "collect"
        else
            event.emit{speed=0, description="explorer", table={id=groupID, free_slots=free_slots}}
            curr_state = "in_base"
        end
    else
        curr_state = "in_base"
    end
end

function cleanUp()
    file = io.open("../data/" .. shared.getNumber("timeStamp") .. "_transporter"..ID.."_group" .. groupID ..".csv", "w")
    for i=1,#dataStep do
        file:write(dataStep[i]..","..dataE[i]..","..dataState[i]..","..dataMem[i]..","..dataW[i]..","..dataTot[i].."\n")
    end
    file:close()
end


-- Helper functions --

function compare_dist(a,b)
    return world.distance({x = PositionX, y = PositionY}, {x = a.x, y = a.y}) > world.distance({x = PositionX, y = PositionY}, {x = b.x, y = b.y})
end
