-- Project:     AM24: Examination Project: Ore Collection on Foreign Planets
-- File:        company.lua
-- Author:      Nicolai A. Lynnerup

agent = require "ranalib_agent"
shared = require "ranalib_shared"

function initializeAgent()
    -- TEST
    say("Company Agent #" .. ID .. " is initialized")

    -- Setup Group(s) --
    -- If all companies cooperate, then only create one group
    if(shared.getTable("world")["M"]) == 1 then
        if(shared.getNumber("groupID") == "no_value") then
            shared.storeNumber("groupID", ID)
        else
            --agent.joinGroup(shared.getNumber("groupID"))
            --say("Agent " .. ID .. " joined Group " .. shared.getNumber("groupID"))
        end
    -- Else create a group for each company
    else
        -- Create group w/ ID (= company ID)
        shared.storeNumber("groupID", ID)
        --agent.joinGroup(ID)
        -- TEST
        --say("Agent " .. ID .. " joined Group " .. shared.getNumber("groupID"))
    end

    -- Add Bases --
    local nBases = shared.getTable("world")["N"]
    for i=1, nBases do
        agent.addAgent("base.lua")
    end
end

function takeStep()
    -- Remove Company Agent
    agent.removeAgent(ID)
end
