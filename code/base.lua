-- Project:     AM24: Examination Project: Ore Collection on Foreign Planets
-- File:        base.lua
-- Author:      Nicolai A. Lynnerup

agent   = require "ranalib_agent"
shared  = require "ranalib_shared"
event   = require "ranalib_event"

joined = false

function initializeAgent()
    say("Base Agent #" .. ID .. " is deployed")

    -- Set color of base
    agent.changeColor(shared.getTable("color")["base"])

    -- Add Explorers --
    local nExplorers = shared.getTable("world")["X"]
    for i=1, nExplorers do
        agent.addAgent("explorer.lua",PositionX,PositionY)
    end

    -- Add Transporters --
    local nTransporters = shared.getTable("world")["Y"]
    for i=1, nTransporters do
        agent.addAgent("transporter.lua",PositionX,PositionY)
    end

    -- Retreive parameters
    C = shared.getTable("world")["C"]

    groupID = shared.getNumber("groupID")

    -- Data Collection
    storage = 0
    step_counter = 0
    data = {}
    step = {}
end

function takeStep()
    --
    if not joined then
        -- Join group
        agent.joinGroup(groupID)
        say("Agent "..ID.." joined group "..groupID)
        joined = true
    end

    table.insert(data,storage)
    table.insert(step,step_counter)
    step_counter = step_counter + 1
end


function handleEvent(sourceX, sourceY, sourceID, eventDescription, eventTable)
    if(eventTable.agentType == "transporter") then
        if (eventTable.cmdType == "offload") and sourceX == PositionX and sourceY == PositionY then
            -- TEST
            --say("Received " .. eventTable.msg .. " ore samples from Agent " .. sourceID)
            if storage < C then
                storage = storage + eventTable.msg
                --say(storage)
            else
                -- Tell agents to come home... We're leaving!
                storage = C
                event.emit{speed=0, description="comehome",table={id=groupID}}
            end
        end
    end
end


function cleanUp()
    file = io.open("/home/nicolai/Documents/AM24/data/comp_P6_I60_600x600_X4_Y4_"..ID..".csv", "w")
    for i=1,#data do
        file:write(data[i].."\n")
    end
    file:close()
end
