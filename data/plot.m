close all; clear; clc;

p2 = csvread('/home/nicolai/Documents/AM24/data/P2_I40_600x600_X4_Y4.csv');
p4 = csvread('/home/nicolai/Documents/AM24/data/P4_I40_600x600_X4_Y4.csv');
p6 = csvread('/home/nicolai/Documents/AM24/data/P6_I40_600x600_X4_Y4.csv');

steps = [1:100000]';

plot(steps,[p2,p4,p6])
title('\fontsize{16} Variation of Perception Scope');
xlabel('Steps');
ylabel('Number of Samples Collected');
legend('P=2','P=4','P=6')

print('/home/nicolai/Documents/AM24/presentation/nlynn12_exam/perception_scope','-depsc');

%%
close all; clear; clc;

I10 = csvread('/home/nicolai/Documents/AM24/data/P6_I10_600x600_X4_Y4.csv');
I20 = csvread('/home/nicolai/Documents/AM24/data/P6_I20_600x600_X4_Y4.csv');
I30 = csvread('/home/nicolai/Documents/AM24/data/P6_I30_600x600_X4_Y4.csv');
I40 = csvread('/home/nicolai/Documents/AM24/data/P6_I40_600x600_X4_Y4.csv');
I50 = csvread('/home/nicolai/Documents/AM24/data/P6_I50_600x600_X4_Y4.csv');

steps = [1:100000]';

plot(steps,[I10,I20,I30,I40,I50])
title('\fontsize{16} Variation of Interaction Scope');
xlabel('Steps');
ylabel('Number of Samples Collected');
legend('I=10','I=20','I=30','I=40','I=50')

print('/home/nicolai/Documents/AM24/presentation/nlynn12_exam/interaction_scope','-depsc');

%% CO-OP
close all; clear; clc;

B1 = csvread('/home/nicolai/Documents/AM24/data/coop_P6_I60_600x600_X4_Y4_3.csv');
B2 = csvread('/home/nicolai/Documents/AM24/data/coop_P6_I60_600x600_X4_Y4_13.csv');
B3 = csvread('/home/nicolai/Documents/AM24/data/coop_P6_I60_600x600_X4_Y4_23.csv');
B4 = csvread('/home/nicolai/Documents/AM24/data/coop_P6_I60_600x600_X4_Y4_33.csv');
B5 = csvread('/home/nicolai/Documents/AM24/data/coop_P6_I60_600x600_X4_Y4_43.csv');
B6 = csvread('/home/nicolai/Documents/AM24/data/comp_P6_I60_600x600_X4_Y4_3.csv');
B7 = csvread('/home/nicolai/Documents/AM24/data/comp_P6_I60_600x600_X4_Y4_13.csv');
B8 = csvread('/home/nicolai/Documents/AM24/data/comp_P6_I60_600x600_X4_Y4_23.csv');
B9 = csvread('/home/nicolai/Documents/AM24/data/comp_P6_I60_600x600_X4_Y4_33.csv');
B10 = csvread('/home/nicolai/Documents/AM24/data/comp_P6_I60_600x600_X4_Y4_43.csv');

steps = [1:100000]';

plot(steps,[B1,B2,B3,B4,B5,B6,B7,B8,B9,B10])
title('\fontsize{16} Co-op vs. Competetive');
xlabel('Steps');
ylabel('Number of Samples Collected');
legend('Co-op B1','Co-op B2','Co-op B3','Co-op B4','Co-op B5','Comp. B1','Comp. B1','Comp. B2','Comp. B1','Comp. B1')

print('/home/nicolai/Documents/AM24/presentation/nlynn12_exam/coop_comp','-depsc');

%%
close all; clear; clc;

b1 = csvread('/home/nicolai/Documents/AM24/data/coop_250x250_X7_Y9_2b_b1.csv');
b2 = csvread('/home/nicolai/Documents/AM24/data/coop_250x250_X7_Y9_2b_b2.csv');
b3 = csvread('/home/nicolai/Documents/AM24/data/comp_250x250_X7_Y9_2b_b1.csv');
b4 = csvread('/home/nicolai/Documents/AM24/data/comp_250x250_X7_Y9_2b_b2.csv');

steps = [1:10000]';

plot(steps,[b1,b2,b3,b4])
title('\fontsize{16} 2 bases in cooperation mode');
xlabel('Steps');
ylabel('Number of Samples Collected');
legend('Coop: Base 1','Coop: Base 2','Comp: Base 1', 'Comp: Base 2')

print('/home/nicolai/Documents/AM24/presentation/nlynn12_exam/b2_coop_vs_comp','-depsc');

%%
close all; clear; clc;

b1 = csvread('/home/nicolai/Documents/AM24/data/250x250_X7_Y9_1b.csv');

steps = [1:10000]';

plot(steps,b1)
title('\fontsize{16} 1 base');
xlabel('Steps');
ylabel('Number of Samples Collected');
legend('Base 1')

print('/home/nicolai/Documents/AM24/presentation/nlynn12_exam/b1_coop_vs_comp','-depsc');